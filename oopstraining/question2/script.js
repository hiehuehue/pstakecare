(function (window, document) {
	var array = document.getElementsByClassName( 'checkbox' );
	var obj = { };
	function Checkbox( elm ) {
		this.elm = elm;
	}
	Checkbox.prototype.check = function () {
		this.elm.checked = true;
	};
	Checkbox.prototype.uncheck = function () {
		this.elm.checked = false;
	};
function lala(i) {
		var checkboxname = "checkbox" + i;
		obj[ checkboxname ] = new Checkbox(array[i]);
	}
	for (var i = 0; i < array.length; i++) {
		lala(i);
	}
	document.getElementById('check').addEventListener('click', function () {
		Object.keys( obj ).forEach( function (checkbox) {
			obj[checkbox].check();
		});
	});
	document.getElementById('uncheck').addEventListener('click', function () {
		Object.keys( obj ).forEach( function (checkbox) {
			obj[checkbox].uncheck();
		});
	});
})(window,document);
//how to create dynamic variable names
