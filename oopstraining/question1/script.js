var User = function User (name, age) {
  this.name = name;
  this.age = age;
  this.compare = function (user) {
    if (user instanceof User) {
      if (user.age === age) {
        console.log(name + ' is same age as ' + user.name);
      }else if (user.age > age) {
        console.log(user.name + ' is older than ' + name);
      }else {
        console.log(name + ' is older than ' + user.name);
      }
    }
  };
};
var user1 = new User('john', 12);
var user2 = new User('mary', 12);

user1.compare(user2);
