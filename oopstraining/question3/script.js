(function (window, document) {
  function Checkboxobj (checkboxobj , checkboxes, nonecheck) {
    this.noofchecks = 0;
    this.checkboxes = checkboxes;
    this.checkboxobj = checkboxobj;
    this.nonecheck = nonecheck;
  }
  Checkboxobj.prototype.setNoOfChecks = function () {
    var self = this;
    self.noofchecks = 0;
    Array.prototype.forEach.call(self.checkboxes, function (checkbox) {
      if (checkbox.checked) {
        self.noofchecks++;
      }
    });
  };
  Checkboxobj.prototype.validate = function () {
    var self = this;
    self.setNoOfChecks();
    return (self.noofchecks >= 4) ? true : false;
  };
  Checkboxobj.prototype.CheckNone = function (flag) {
    var self = this;
    self.nonecheck.checked = flag;
  };
  Checkboxobj.prototype.initEventListeners = function () {
    var self = this;
    self.checkboxobj.addEventListener('change' , function (e) {
      self.setNoOfChecks();
      if (e.target.nodeName === 'INPUT' && e.target.getAttribute('type') === 'checkbox') {
        if (self.validate()) {
          e.target.checked = false;
          alert('donot check more than 3 checkboxes');
        }else {
          self.setNoOfChecks();
          self.CheckNone(false);
        }
      }
    });
    self.nonecheck.addEventListener('change' , function (e) {
      Array.prototype.forEach.call(self.checkboxes, function (checkbox) {
        if (checkbox.checked) {
          checkbox.checked = false;
        }
      });
      self.setNoOfChecks();
    });
  };
  Checkboxobj.prototype.init = function () {
    this.setNoOfChecks();
    this.initEventListeners();
  };
  var checkboxobj = document.getElementsByClassName('checkbox')[0],
    checkboxes = checkboxobj.getElementsByClassName('check'),
    nonecheck = document.getElementsByClassName('none')[0],
    checkboxblock = new Checkboxobj(checkboxobj, checkboxes, nonecheck);
  checkboxblock.init();
})(window, document);
