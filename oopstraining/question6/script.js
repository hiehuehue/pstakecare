(function () {
  function Alerter (textfield) {
    var self = this;
    self.textfield = textfield;
    function validateName (name) {
      name = String(name).trim();
      return (/^[a-zA-Z]+$/.test(name)) ? true : false;
    }
    function promptForName (alert) {
      return prompt(alert);
    }
    Alerter.prototype.init = function () {
      var fullName = '';
      var firstName = promptForName('What is Your first Name?');
      while (!validateName(firstName)) {
        firstName = promptForName('Use only letters to form your first name\nNowhitespace characters in between\nWhat is Your first Name?');
      }
      var lastName = promptForName('What is Your last Name?');
      while (!validateName(lastName)) {
        lastName = promptForName('Use only letters to form your first name\nNowhitespace characters in between\nWhat is Your Last Name?');
      }
      fullName = 'Hello ' + firstName + ' ' + lastName;
      document.getElementsByClassName('div_alert_text')[0].textContent = fullName;
    };
  }
  var textfield = document.getElementsByClassName('div_alert_text')[0];
  var alerter = new Alerter(textfield);
  alerter.init();
})();
