(function (window, document) {
  function Formbox () {
    var self = this;
    self.button = button;
    function loginValidate (value) {
      console.log(value);
      var regex = /[a-zA-Z0-9]/;
      return regex.test(value);
    }
    function emailValidate (value) {
      var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      return regex.test(value);
    }
    function nameValidate (value) {
      var regex = /[a-zA-Z_-]/;
      return regex.test(value);
    }
    function parseUrl (url) {
      var regex = /(?:https?:\/\/)?(?:([A-Za-z0-9]*)\.)?([A-Za-z0-9]*)\.([^\/]*)(.*)/;
      return regex.test(url);
    }
    function textValidate (value) {
      var regex = /^[^]{50,}$/;
      return regex.test(value);
    }
    function checkboxCheck (checkbox) {
      return checkbox;
    }
    Formbox.prototype.validate = function () {
      var self = this;
      if (!(loginValidate(self.login) && emailValidate(self.email) && nameValidate(self.name) && parseUrl(self.url) && checkboxCheck(self.check) && textValidate(self.text))) {
        if (!loginValidate(self.login)) {
          alert('login field must contain atleast a number or alphabet');
        }
        if (!emailValidate(self.email)) {
          alert('email field must contain a valid email');
        }
        if (!nameValidate(self.name)) {
          alert("name must contain alphabets or '-' or '_'");
        }
        if (!parseUrl(self.url)) {
          alert('Write a url in the form :\n(http(s)://)(anything.)something.someotherthing');
        }
        if (!textValidate(self.text)) {
          alert("Please write 50 characters in 'About me'");
        }
        if (!checkboxCheck(self.check)) {
          alert('Please check the checkbox before proceeding');
        }
      }else {
        alert('received notification');
      }
    };
  }
  Formbox.prototype.load = function (login, button, name, email, zone, url, text, check) {
    var self = this;
    self.login = login;
    self.email = email;
    self.name = name;
    self.zone = zone;
    self.url = url;
    self.text = text;
    self.check = check;
    console.log(self);
  };
  Formbox.prototype.initFormBox = function () {
    var self = this;
    var button = document.getElementById('button');
    button.addEventListener('click' , function () {
      var login = document.getElementById('login').value,
        email = document.getElementById('mail').value,
        name = document.getElementById('name').value,
        zone = document.getElementById('zone').selectedOptions[0].value,
        url = document.getElementById('page').value,
        text = document.getElementById('text').value,
        checkbox = document.getElementById('checkbox').checked;
      self.load(login, button, name, email, zone, url, text, checkbox);
      self.validate();
    });
  };
  var formbox = new Formbox();
  formbox.initFormBox();
})(window, document);
