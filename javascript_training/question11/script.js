// TODO: Create a simple form with a textfield and a submit button. When I enter a URL in this text field and submit the form, it should alert me the domain name from the entered url. For example, If I enter:
// 1. http://example.com/folder/file.html, then on submitting the form it should alert something like: Domain: example.com.
// 2. (with subdomain) http://pstakecare.example.com/folder/file.html, then on submitting the form it should alert something like:
// Domain: example.com, Subdomain: pstakecare.
// Basic Idea is to extract domain name and subdomain from a URL.
// PS: Use Regular Expressions for this exercise.(Please use only Object oriented approach to write your code.)
// TODO: incarporate ports in url ex:(localhost:3000)
(function () {
	function parseUrl(url) {
		var regex = /(?:https?:\/\/)?(?:([A-Za-z0-9]*)\.)?([A-Za-z0-9]*)\.([^\s\/]*)(.*)/;
		return regex.exec(url);
	}
	function createAlert(regexarr) {
		if(regexarr instanceof Array){
			var text = 'Domain: '+ regexarr[2]+'.'+regexarr[3] + '\n Subdomain: '+regexarr[1];
			alert(text);
		}else{
			alert("Write a url in the form :\n(http(s)://)(anything.)something.someotherthing");
		}
	}
	function init() {
		var button = document.getElementById('button');
		button.addEventListener('click',function () {
			var url = document.getElementById('url').value;
			var regexarr = parseUrl(url);
			console.log(regexarr);
			createAlert(regexarr);
		});
	}
	document.addEventListener("DOMContentLoaded",init);
})();
