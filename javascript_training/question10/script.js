// TODO:  Create a form with two text fields labeled "Enter Number" and "Result". When I enter something in first text field and submit the form, it should write true in second text field if entered value is numeric otherwise false.
// Also if entered value is not numeric, form should not be submitted.(Please use only Object oriented approach to write your code.)

(function () {
	function checkNumber() {
		var number = document.getElementById('number');
		if(number.value&&!(Number(number.value) !== Number(number.value))){
			return true;
		}else{
			return false;
		}
	}
	function changeResult() {
		var result = document.getElementById('result');
		console.log(checkNumber());
		if(checkNumber()){
			result.value = "true";
		}else{
			result.value = "false";
		}
	}
	function init() {
		var submitbutton = document.getElementById('submit');
		submitbutton.addEventListener("click",function () {
			changeResult();
		});
	}
	document.addEventListener("DOMContentLoaded",init);
})();
