// TODO Create a HTML page, when it opens in Browser, it should prompt twice. Once for first name and second for last name. When you enter first and last name, it should alert you as "Hello firstname lastname."
// After alert it should put that text in html page.
// PS: Keep in mind when you take inputs from users, always consider cases where user has entered null or blank values(few white spaces). User may press ESC button. In such cases you should not accept the input.

(function(){
	function validateName(name) {
		name = String(name).trim();
		if(/^[a-zA-Z]+$/.test(name)){
			return true;
		}else{
			return false;
		}
	}
	function promptForName(alert) {
		return prompt(alert);
	}
	function init() {
		var fullName = '';
		var firstName = promptForName("What is Your first Name?");
		while (!validateName(firstName)) {
			firstName = promptForName("Use only letters to form your first name\nNowhitespace characters in between\nWhat is Your first Name?");
		}
		var lastName = promptForName("What is Your last Name?");
		while (!validateName(lastName)) {
			lastName = promptForName("Use only letters to form your first name\nNowhitespace characters in between\nWhat is Your Last Name?");
		}
		fullName ='Hello '+firstName+' ' + lastName;
		document.getElementsByClassName('div_alert_text')[0].textContent = fullName;
	}

	document.addEventListener("DOMContentLoaded", init);
})();
