// TODO Create a list of checkbox (red, green, yellow, white, black). Now create two links "Check all" and 'none'. When you click on check all link all checkbox will be selected, and when you click on none link all checkbox will be deselected(working example is gmail).
(function(){
	var $checkbutton = document.getElementById('check');
	var $uncheckbutton = document.getElementById('uncheck');
	var $checkboxdiv = document.getElementsByClassName('checkbox')[0];
	var $checkboxes = $checkboxdiv.getElementsByClassName('check');
	// console.log($checkboxes[0]);
	function checkAll(checkboxes){
		for(var i =0; i<checkboxes.length; i++){
			checkboxes[i].checked = true;
		}
	}
	function checkNone(checkboxes){
		for(var i =0; i<checkboxes.length; i++){
			checkboxes[i].checked = false;
		}
	}
	function init() {
		$checkbutton.addEventListener("click",function() {
			checkAll($checkboxes);
		},false);
		$uncheckbutton.addEventListener("click",function(){
			checkNone($checkboxes);
		},false);

	}
	document.addEventListener("DOMContentLoaded", init);
})();
