// TODO: Create a form like (https://drive.google.com/drive/u/1/folders/0BzJOEUY84GQ0UGNkNEhpSjBSQU0) (IMAGE: form_validation.png )and add JS validations to that form.When you click on 'Go' JS will check all text fields, and give an alert like "Login Id can't be empty". One alert per empty field.
// For textarea it should check for empty and min length of 50 characters.
// If any field is invalid then it should not submit the form. Also It should also confirm "receive notification"
       
// The form you created in this (https://drive.google.com/drive/u/1/folders/0BzJOEUY84GQ0UGNkNEhpSjBSQU0 )(IMAGE: form_validation.png )
// Add validations for the correct format of email and homepage(url) using regular expressions. (Please use only Object oriented approach to write your code.)
(function () {
	function validate() {
		var $login = document.getElementById('login').value,
			$email = document.getElementById('mail').value,
			$name = document.getElementById('name').value,
			$zone = document.getElementById('zone').selectedOptions[0].value,
			$url = document.getElementById('page').value,
			$text = document.getElementById('text').value,
			$checkbox = document.getElementById('checkbox').checked;

		function loginValidate(value) {
			var regex = /[a-zA-Z0-9]/;
			return regex.test(value);
		}
		function emailValidate(value) {
			var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
			return regex.test(value);
		}
		function nameValidate(value) {
			var regex = /[a-zA-Z_-]/;
			return regex.test(value);
		}
		function parseUrl(url) {
			var regex = /(?:https?:\/\/)?(?:([A-Za-z0-9]*)\.)?([A-Za-z0-9]*)\.([^\/]*)(.*)/;
			return regex.test(url);
		}
		function textValidate(value) {
			var regex =/^[^]{50,}$/;
			return regex.test(value);
		}
		function checkboxCheck(checkbox) {
			return checkbox;
		}
		if(!(loginValidate($login)&&emailValidate($email)&&nameValidate($name)&&parseUrl($url)&&checkboxCheck($checkbox)&&textValidate($text))){
			if(!loginValidate($login)){
				alert('login field must contain atleast a number or alphabet');
			}
			if(!emailValidate($email)){
				alert('email field must contain a valid email');
			}
			if(!nameValidate($name)){
				alert("name must contain alphabets or '-' or '_'");
			}
			if(!parseUrl($url)){
				alert("Write a url in the form :\n(http(s)://)(anything.)something.someotherthing");
			}
			if(!textValidate($text)){
				alert("Please write 50 characters in 'About me'");
			}
			if(!checkboxCheck($checkbox)){
				alert("Please check the checkbox before proceeding");
			}
		}else {
			alert("received notification");
		}

	}
	function init() {
		$button = document.getElementById('button');
		$button.addEventListener('click',function () {
			validate();
		});
	}
	window.addEventListener('DOMContentLoaded',init);
})()
