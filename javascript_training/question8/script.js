(function () {
	function glowselect(e){
		if(e.target.classList.contains('selected')){
			e.target.classList.remove('selected')
		}else{
			e.target.classList.add('selected');
		}
	}
	function addItemFromTo(selected, fromlist, tolist, i) {
		i++;
		if(selected.length !== 0){
			var select = selected[0];
			select.classList.remove('selected');
			select.parentElement.removeChild(select);
			tolist.insertBefore(select,tolist.firstChild);
			addItemFromTo(selected, fromlist, tolist);
		}else {
			if(i===1)alert('Select a box');
		}
	}
	function init() {
		var $add = document.getElementsByClassName('add')[0];
		var $remove = document.getElementsByClassName('remove')[0];
		var $leftlist = document.getElementsByClassName('left-listbox')[0];
		var $rightlist = document.getElementsByClassName('right-listbox')[0];
		var $glowselect = document.getElementsByClassName('list-item');
		$add.addEventListener('click',function () {
			var $selected = $leftlist.getElementsByClassName('selected');
			addItemFromTo($selected, $leftlist, $rightlist, 0);
		});
		$remove.addEventListener('click',function () {
			var $selected = $rightlist.getElementsByClassName('selected');
			addItemFromTo($selected, $rightlist, $leftlist, 0);
		});
		for (var i = 0; i < $glowselect.length; i++) {
			$glowselect[i].addEventListener('click',glowselect);
		}
	}
	window.addEventListener('DOMContentLoaded',init);
})()
