// TODO Create a list of checkboxes(Sunday, Monday, Wednesday, Thursday, Tuesday, Friday, Saturday and none). Now when I start checking these checkboxes, it should not let me check more than 3 checkboxes. When I try to check on 4th checkbox it should alert me as "Only 3 days can be selected. You have already selected sunday, monday and tuesday".
// Now when I click on none, all other checkboxes should be deselected. Also when 'none' is selected and I select any other option(lets assume sunday), that option(sunday) will be selected and none will be deselected.
(function(){
	var $checkboxdiv = document.getElementsByClassName('checkbox')[0];
	var $checkboxes = $checkboxdiv.getElementsByClassName('check');
	var $nonecheck = document.getElementsByClassName('none')[0];
	var arrtextday =[];
	for (var i = 0; i < $checkboxes.length; i++) {
		arrtextday.push($checkboxes[i].parentNode.childNodes[1].nodeValue);
	}
	function checkNone(checkboxes){
		for(var i =0; i<checkboxes.length; i++){
			checkboxes[i].checked = false;
		}
	}
	function countNoOfChecksandAlertMsg(checkboxes){
		var noofchecks = 0;
		var checkeddays = [];
		var alertMsg = 'Only 3 days can be selected. You have already selected '
		for(var i=0; i<checkboxes.length; i++){
			if(checkboxes[i].checked){
				noofchecks++;
				checkeddays.push(arrtextday[i]);
			}
		}
		alertMsg += checkeddays[0]+', '+checkeddays[1]+' and '+checkeddays[2];
		return {
			'noOfChecks':noofchecks,
			'alertMsg':alertMsg
		}
	}
	function checkBoxEventHandler() {
		$nonecheck.checked = false;
		var checksandalert = countNoOfChecksandAlertMsg($checkboxes);
		if(checksandalert.noOfChecks>=4){
			this.checked = false;
			alert(checksandalert.alertMsg);
		}
	}
	function init() {
		for (var i = 0; i < $checkboxes.length; i++) {
			$checkboxes[i].addEventListener('change', checkBoxEventHandler);
		}
		$nonecheck.addEventListener('change',function(){
			checkNone($checkboxes);
		});
	}
	document.addEventListener("DOMContentLoaded", init);
})();
